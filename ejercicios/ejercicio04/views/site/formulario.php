<?php

use yii\helpers\Html;

echo Html::beginForm(['site/formulario'], 'post');
echo Html::beginTag('div',['class'=>'form-gruop']);
echo Html::label('Destinatario', 'iEmail',['class'=>'control-label']);
echo Html::input('email','email','',['id'=>'iEmail','class'=>'form-control']);
echo Html::label('Asunto', 'iAsunto',['class'=>'control-label']);
echo Html::input('text','asunto','',['id'=>'iEmail','class'=>'form-control']);
echo Html::label('Mensaje', 'iMensaje',['class'=>'control-label']);
echo Html::input('textarea','mensaje','',['id'=>'iEmail','class'=>'form-control']);
echo Html::endTag('div');
echo Html::submitButton('Enviar', ['class'=>'btn btn-info']);
echo Html::endForm();

echo $this->render('formulario');
