<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $email;
    public $subject;
    public $body;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['email', 'subject', 'body'], 'required','message' =>'el campo {attribute} es obligatorio'],
            ['email', 'email', 'message' =>'el formato del correo no es obligatorio'],

            ['email', 'string','max' =>30,'message' =>'el correo es demasiado largo'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'email' => 'Correo Destinatario',
            'subject' => 'Asunto',
            'body' => 'Texto'
        ];
    }

}
