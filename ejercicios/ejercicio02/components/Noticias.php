<?php

namespace app\components;

use yii\base\Widget;
use yii\helpers\Html;

/**
 * Class Noticias
 * @author Rubén
 */
class Noticias extends Widget
{
    public $titulo;

    /**
     * undocumented function
     *
     * @return void
     */
    public function init()
    {
        parent::init();
        echo '<div class="panel panel-default">';
        echo '<div class="panel-heading">';
        echo Html::tag('h3', Html::encode("$this->titulo"),['class'=>"panel-title"]);
        echo "</div>";
        echo '<div class="panel-body">';
    }

    /**
     * undocumented function
     *
     * @return void
     */
    public function run()
    {
        return '</div></div>';
    }
    
}
