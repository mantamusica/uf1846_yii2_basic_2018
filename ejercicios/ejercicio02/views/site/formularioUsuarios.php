<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin();
echo $form->field($model, 'nombre');
echo $form->field($model, 'apellidos');
echo $form->field($model, 'edad');
echo $form->field($model, 'email');

echo '<div class="form-group">';

echo Html::submitButton('submit', ['class'=>'btn btn-primary']);

echo '</div>';

ActiveForm::end();
?>
