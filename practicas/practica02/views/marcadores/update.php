<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Marcadores */

?>
<div class="marcadores-update">

    <?= $this->render('_form', [
        'model' => $model,
        'boton' => ['Eliminar' => Url::to(['marcadores/delete', 'id'=> $model->id])]
    ]) ?>

</div>
