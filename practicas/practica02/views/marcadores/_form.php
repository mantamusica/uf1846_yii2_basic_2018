<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Marcadores */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="marcadores-form">

<?php $form = ActiveForm::begin([
    'layout'=>'horizontal'
]); ?>

    <?= $form->field($model, 'enlace')->textInput([
        'maxlength' => true,
    ]) ?>
        
    <?= $form->field($model, 'descripcion_corta')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'descripcion_larga')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'publico')->dropDownList(['si','no'],['prompt'=>'Selecciona una opción']) ?>

    <div class="form-group pull-right">
<?php if (isset($boton) && is_array($boton)){
foreach ($boton as $key => $value) {
   echo Html::a($key, $value, ['class'=>'btn btn-primary']);
}
}?>
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
