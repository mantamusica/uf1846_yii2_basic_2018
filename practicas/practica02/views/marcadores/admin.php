<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Marcadores';

echo '<div class=row>';
echo '<a class="btn btn-primary col-sm-2 col-md-offset-10" href="';
echo 'nuevo" role="button">Nuevo</a>';
echo '</div>';

echo $this->render('index',[
    'dataProvider' => $dataProvider,
    'vista' => '_admin'
] );

echo '<div class=row>';
echo '<a class="btn btn-primary col-sm-2 col-md-offset-10" href="';
echo 'create" role="button">Nuevo</a>';
echo '</div>';

