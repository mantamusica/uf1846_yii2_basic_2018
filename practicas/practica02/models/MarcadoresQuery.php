<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Marcadores]].
 *
 * @see Marcadores
 */
class MarcadoresQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Marcadores[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Marcadores|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
