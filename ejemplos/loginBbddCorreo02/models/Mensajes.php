<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mensajes".
 *
 * @property int $id
 * @property string $autor
 * @property string $mensaje
 */
class Mensajes extends \yii\db\ActiveRecord
{
    public $verifyCode;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mensajes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['autor', 'mensaje'], 'required'],
            [['autor', 'mensaje'], 'string', 'max' => 128],
            ['verifyCode', 'captcha'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'autor' => 'Autor',
            'mensaje' => 'Mensaje',
        ];
    }

    /**
     * @inheritdoc
     * @return MensajesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MensajesQuery(get_called_class());
    }
}
