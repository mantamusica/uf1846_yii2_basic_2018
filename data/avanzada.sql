-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 30-01-2018 a las 10:07:16
-- Versión del servidor: 10.1.26-MariaDB
-- Versión de PHP: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `avanzada`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categories`
--

CREATE TABLE `categories` (
  `id` tinyint(4) NOT NULL,
  `name` varchar(128) COLLATE utf8_spanish_ci NOT NULL,
  `color` varchar(64) COLLATE utf8_spanish_ci NOT NULL,
  `active` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `categories`
--

INSERT INTO `categories` (`id`, `name`, `color`, `active`) VALUES
(1, 'priority one', 'white', 1),
(2, 'priority two', 'blue', 1),
(3, 'priority three', 'orange', 1),
(4, 'priority four', 'pink', 0),
(5, 'priority five', 'red', 1),
(6, 'priority six', 'grey', 0),
(7, 'priority seven', 'black', 0),
(8, 'priority eight', 'brown', 0),
(9, 'priority ten', 'yellow', 0),
(10, 'prueba category', 'undefined', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `chores`
--

CREATE TABLE `chores` (
  `id` int(11) NOT NULL,
  `name` varchar(128) COLLATE utf8_spanish_ci NOT NULL,
  `expiration_date` datetime NOT NULL,
  `creation_date` datetime NOT NULL,
  `alarm` tinyint(1) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_category` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `chores`
--

INSERT INTO `chores` (`id`, `name`, `expiration_date`, `creation_date`, `alarm`, `active`, `id_user`, `id_category`) VALUES
(1, 'prueba uno', '2018-01-23 08:00:19', '2018-01-01 00:00:00', 1, 1, 17, 5),
(2, 'prueba dos', '2018-01-31 00:00:00', '2018-01-01 00:00:00', 1, 1, 8, 9),
(3, 'prueba tres', '2017-12-31 22:30:49', '2018-01-01 03:30:49', 1, 1, 17, 4),
(4, 'test doce', '2018-01-31 00:00:00', '2018-01-02 00:00:00', 0, 1, 8, 9),
(5, 'test trece', '2018-01-02 00:00:00', '2018-01-26 00:00:00', 1, 1, 94, 4),
(6, 'prueba cotarce', '2018-01-04 00:00:00', '2018-01-01 00:00:00', 0, 0, 88, 1),
(7, 'prueba quince', '2018-01-29 00:00:00', '2018-01-31 00:00:00', 1, 1, 8, 2),
(8, 'prueba otra más', '2018-01-17 00:00:00', '2018-01-06 00:00:00', 1, 1, 94, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `id` tinyint(4) NOT NULL,
  `nombre` varchar(128) NOT NULL,
  `apellidos` varchar(128) NOT NULL,
  `direccion` varchar(128) NOT NULL,
  `email` varchar(256) NOT NULL,
  `socio` tinyint(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`id`, `nombre`, `apellidos`, `direccion`, `email`, `socio`) VALUES
(1, 'jose ramon', '', 'calle los dolores torcidos', 'josetoriciza@hotmail.com', 2),
(2, 'jose federico jimenez', '', 'calle sotomayor', 'josekl|@gmial.com', 2),
(3, 'fgbv', '', '', '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mensajes`
--

CREATE TABLE `mensajes` (
  `id` tinyint(4) NOT NULL,
  `autor` varchar(128) NOT NULL,
  `mensaje` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `mensajes`
--

INSERT INTO `mensajes` (`id`, `autor`, `mensaje`) VALUES
(1, 'ramon', 'ogh'),
(2, 'chema', 'jg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `socios`
--

CREATE TABLE `socios` (
  `id` tinyint(10) NOT NULL,
  `nombre` varchar(128) NOT NULL,
  `apellidos` varchar(128) NOT NULL,
  `foto` varchar(128) NOT NULL,
  `edad` tinyint(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `socios`
--

INSERT INTO `socios` (`id`, `nombre`, `apellidos`, `foto`, `edad`) VALUES
(1, 'anuska', 'gollum', 'u1.jpg', 38),
(2, 'silvia', 'terremoto', 'u2.jpg', 37),
(3, 'ruben', 'toca', 'u3.jpg', 47),
(4, 'anuska', 'gollum', 'u4.jpg', 38),
(5, 'lucia', 'pinte', 'u5.jpg', 26),
(6, 'agustin', 'capitan', 'u6.jpg', 41),
(7, 'fran', 'wacha', 'u7.jpg', 23);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `usuario` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `codigo` varchar(256) CHARACTER SET utf8 NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB AVG_ROW_LENGTH=16384 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `usuario`, `password`, `codigo`, `email`, `active`) VALUES
(6, 'admin', 'admin', 'd0ca4cfa7b63cd2d0c204166d7a3521e0941e5cb', 'admidn@admin@gmail.com', 1),
(7, 'manolete', 'manolete', 'd0ca4cfa7b63cd2d0c204166d7a3521e0941e5cb', 'admin@admin@gmail.com', 1),
(8, 'chema', 'chema', '1430170bc4463816eef89a902d9b948629a7b7c9', 'chemacagigas@gmail.com', 1),
(17, 'ramon', 'ramon', '2bfe2630cd649d2b7fecaff845d0a28fb600f4a0', 'alpeformacion@gmail.com', 1),
(88, 'babion', 'babion', 'd0ca4cfa7b63cd2d0c204166d7a3521e0941e5cb', 'babion@gmail.com', 1),
(94, 'van', '456123', '', 'jklaf@fdas.com', 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `chores`
--
ALTER TABLE `chores`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_chores_categories_id` (`id_category`),
  ADD KEY `FK_chores_usuarios_id` (`id_user`);

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_clientes_socios_id` (`socio`);

--
-- Indices de la tabla `mensajes`
--
ALTER TABLE `mensajes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `socios`
--
ALTER TABLE `socios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categories`
--
ALTER TABLE `categories`
  MODIFY `id` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `chores`
--
ALTER TABLE `chores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `mensajes`
--
ALTER TABLE `mensajes`
  MODIFY `id` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `socios`
--
ALTER TABLE `socios`
  MODIFY `id` tinyint(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `chores`
--
ALTER TABLE `chores`
  ADD CONSTRAINT `FK_chores_categories_id` FOREIGN KEY (`id_category`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_chores_usuarios_id` FOREIGN KEY (`id_user`) REFERENCES `usuarios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD CONSTRAINT `FK_clientes_socios_id` FOREIGN KEY (`socio`) REFERENCES `socios` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
